#include <math.h>
#include <fstream>
#include <stdio.h>
#include <string>

#include <CL/cl.hpp>
#include "utility.h"

static const cl_uint vectorSize = 4096; //must be evenly divisible by workSize
static const cl_uint workSize = 256;

int main(void)
{
	cl_int err;

	//Setup Platform
	//Get Platform ID
	std::vector<cl::Platform> PlatformList;

	err = cl::platform::get(&PlatformList);
	checkErr(err, "Get Platform List");
	checkErr(PlatformList.size()>=1 ? CL_SUCCESS : -1, "cl::Platform::get");
	print_platform_info(&PlatformList);
	//Look for Fast Emulation Platform
	uint current_platform_id=get_platform_id_with_string(&PlatformList, "Emulation");
	printf("Using Platform: %d\n\n", current_platform_id);
	
	//Setup Device
	//Get Device ID
	std::vector<cl::Device> DeviceList;

	err = PlatformList[current_platform_id].getDevices(
		CL_DEVICE_TYPE_ALL,&DeviceList
	);
	checkErr(err, "Get Devices");
	print_device_info(&DeviceList);
	
	//Create Context
	cl::Context MyContext(
		DeviceList,
		NULL,
		NULL,
		NULL,
		&err
	);
	checkErr(err, "Context Constructor");
	
	//Create Command queue
	cl::CommandQueue MyQueue(
		MyContext,
		DeviceList[0],
		0,
		NULL
	);
	checkErr(err, "Queue Constructor");

	//Create Buffers for input and output
	cl::Buffer Buffer_In( &MyContext, CL_MEM_READ_ONLY, sizeof(cl_float)*vectorSize,NULL;&err);
	
	cl::Buffer Buffer_In2( &MyContext, CL_MEM_READ_ONLY, sizeof(cl_float)*vectorSize,NULL;&err);
	
	cl::Buffer Buffer_Out( &MyContext, CL_MEM_WRITE_ONLY, sizeof(cl_float)*vectorSize,NULL;&err);
	

	//Inputs and Outputs to Kernel, X and Y are inputs, Z is output
	//The aligned attribute is used to ensure alignment
	//so that DMA could be used if we were working with a real FPGA board
	cl_float X[vectorSize]  __attribute__ ((aligned (64)));
	cl_float Y[vectorSize]  __attribute__ ((aligned (64)));
	cl_float Z[vectorSize]  __attribute__ ((aligned (64)));

	//Allocates memory with value from 0 to 1000
	cl_float LO= 0;   cl_float HI=1000;
	fill_generate(X, Y, Z, LO, HI, vectorSize);

	//Write data to device
	err = MyQueue.enqueueWriteBuffer(Buffer_In,CL_FALSE,0,sizeof(cl_float)*vectorSize,X);
	checkErr(err, "WriteBuffer");
	err = MyQueue.enqueueWriteBuffer(Buffer_In2,CL_FALSE,0,sizeof(cl_float)*vectorSize,X);
	checkErr(err, "WriteBuffer 2");
	myqueue.finish();

	// create the kernel
	const char *kernel_name = "SimpleKernel";

	//Read in binaries from file
	std::ifstream aocx_stream("SimpleKernel.aocx", std::ios::in|std::ios::binary);
	checkErr(aocx_stream.is_open() ? CL_SUCCESS:-1, "SimpleKernel.aocx");
	std::string prog(std::istreambuf_iterator<char>(aocx_stream), (std::istreambuf_iterator<char>()));
	cl::Program::Binaries mybinaries (1, std::make_pair(prog.c_str(), prog.length()));

	// Create the Program from the AOCX file.

	cl::Program program(MyContext, DeviceList, mybinaries, NULL, &err);
	checkErr(err, "Program Constructor");

	/*"If we were not running in emulation mode, the variable named “mybinaries”
	would store the information from the .aocx file used to program the FPGA.
	However, in the emulation mode, the aocx file is just a software library that
	can be dynamically linked with the host code" -Text taken from the project guide*/

	err=program.build(DeviceList);
	checkErr(err, "Build Program");

	// create the kernel
	//////////////       Find Kernel in Program
	cl::Kernel kernel(program, kernel_name, &err);
	checkErr(err, "Kernel Creation");

	//////////////     Set Arguments to the Kernels
	err = kernel.setArg(0, Buffer_In);
	checkErr(err, "Arg 0");
	err = kernel.setArg(1, Buffer_In2);
	checkErr(err, "Arg 1");
	err = kernel.setArg(2, Buffer_Out);
	checkErr(err, "Arg 2");
	err = kernel.setArg(3, vectorSize);
	checkErr(err, "Arg 3");


	printf("\nLaunching the kernel...\n");
	
	// Launch Kernel
	err=MyQueue.enqueueTask(kernel);
	checkErr(err, "Kernel Execution");

	// read the output
	err=MyQueue.enqueueReadBuffer(Buffer_Out, CL_TRUE, 0, sizeof(cl_float)*vectorSize, Z);
	checkErr(err, "Read Buffer");

	err=myqueue.finish();
	checkErr(err, "Finish Queue");
	
	float CalcZ[vectorSize];

	for (uint i=0; i<vectorSize; i++)
	{
		CalcZ[i] = X[i] + Y[i]; 
				
	}

	//Print Performance Results
	verification (X, Y, Z, CalcZ, vectorSize);


    return 1;
}
